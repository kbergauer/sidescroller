﻿using UnityEngine;
using System.Collections;

#if UNITY_EDITOR
using UnityEditor;
using System.IO;
#endif

public class CItemDatabase : ScriptableObject 
{
	#region PUBLIC VARIABLES

	public CItem[] Items;

	#endregion
	
	#region PROTECTED VARIABLES
	#endregion
	
	#region PRIVATE VARIABLES
	#endregion
	
	
	
	#region MONOBEHAVIOUR
	#endregion
	
	
	
	#region PUBLIC METHODS

	#if UNITY_EDITOR
	
	[MenuItem("Assets/Create/Item Database")]
	public static void CreateAsset()
	{
		CItemDatabase asset = ScriptableObject.CreateInstance<CItemDatabase>();
		
		string path = UnityEditor.AssetDatabase.GetAssetPath(UnityEditor.Selection.activeObject);
		if(path == "")
		{
			path = "Assets";
		}
		else if(Path.GetExtension(path) != "")
		{
			path = path.Replace(Path.GetFileName(UnityEditor.AssetDatabase.GetAssetPath(UnityEditor.Selection.activeObject)), "");
		}
		
		string assetPathAndName = UnityEditor.AssetDatabase.GenerateUniqueAssetPath(path + "/New Item Database.asset");
		
		UnityEditor.AssetDatabase.CreateAsset(asset, assetPathAndName);
		
		UnityEditor.AssetDatabase.SaveAssets();
		//UnityEditor.EditorUtility.FocusProjectWindow();
		UnityEditor.Selection.activeObject = asset;
	}
	
	#endif

	#endregion
	
	
	
	#region PRIVATE METHODS
	#endregion
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class CInventory 
{
	#region EVENTS

	public delegate void InventoryEventHandler(CItem _Item);
	public event InventoryEventHandler OnAddItem;
	public event InventoryEventHandler OnUpdateItemStack;

	#endregion

	#region PUBLIC VARIABLES

	public int BagSize;
	public List<CItem> Bag;

	#endregion
	
	#region PROTECTED VARIABLES
	#endregion
	
	#region PRIVATE VARIABLES
	#endregion

	#region PROPERTIES

	public bool IsBagEmpty
	{
		get { return Bag.Count == 0; }
	}

	public bool IsBagFull
	{
		get { return Bag.Count == BagSize; }
	}

	#endregion
	
	
	
	#region CONSTRUCTORS

	public CInventory()
	{
		BagSize = 0;
		Bag = new List<CItem>();
	}

	public CInventory(int _BagSize)
	{
		BagSize = _BagSize;
		Bag = new List<CItem>();
	}

	#endregion
	
	
	
	#region PUBLIC METHODS

	public bool AddItem(CItem _Item)
	{
		// Do not pick up anything if bag is already full
		if(IsBagFull)
			return false;

		bool add = true;

		// Check for existing stack
		if(_Item.ID >= 0 && _Item.StackSize > 1)
		{
			for(int i = 0; i < Bag.Count; i++)
			{
				// Find fillable stack of the same item id
				if(Bag[i].ID == _Item.ID && Bag[i].Amount < Bag[i].StackSize)
				{
					Bag[i].Amount += _Item.Amount;

					// Trigger event
					if(OnUpdateItemStack != null)
						OnUpdateItemStack(Bag[i]);

					add = false;
					break;
				}
			}
		}

		// Add new item
		if(add)
		{
			Bag.Add(_Item);

			// Trigger event
			if(OnAddItem != null)
				OnAddItem(_Item);
		}

		return true;
	}

	#endregion
	
	
	
	#region PRIVATE METHODS
	#endregion
}

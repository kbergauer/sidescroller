﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CItemFactory : MonoBehaviour 
{
	#region PUBLIC VARIABLES
	#endregion
	
	#region PROTECTED VARIABLES
	#endregion
	
	#region PRIVATE VARIABLES
	#endregion
	
	
	
	#region MONOBEHAVIOUR
	#endregion
	
	
	
	#region PUBLIC METHODS

	public static CItem GetRandom(CItem.EType _Type)
	{
		CItem result;

		Object[] assets = Resources.LoadAll("Icons/Junk");
		Sprite sprite = null;
		List<Sprite> sprites = new List<Sprite>();
		for(int i = 0; i < assets.Length; i++)
		{
			sprite = assets[i] as Sprite;
			if(sprite != null)
				sprites.Add(sprite);
		}

		switch(_Type)
		{
		case CItem.EType.Junk:
		{
			result = new CItem(-1, "Junk", "Some useless crap.", CItem.EType.Junk, Random.Range(0.0f, 100.0f), sprites[Random.Range(0, sprites.Count)]);
		} break;

		default:
		{
			result = new CItem();

		} break;
		}

		return result;
	}

	public static CItem Get(int _ID)
	{
		// Get database
		CItemDatabase db = Resources.Load<CItemDatabase>("ItemDatabase"); 
		if(db == null)
		{
			Debug.LogError("CItemFactory: Unable to locate item database asset in resources.");
			return null;
		}

		// Check id validity
		if(db.Items.Length <= _ID)
		{
			Debug.LogError("CItemFactory: Requesting invalid item id (" + _ID + ").");
			return null;
		}

		// Return database copy
		CItem item = db.Items[_ID].Copy();
		item.ID = _ID;
		return item;
	}

	#endregion
	
	
	
	#region PRIVATE METHODS
	#endregion
}

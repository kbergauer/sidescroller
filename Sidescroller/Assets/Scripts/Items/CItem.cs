﻿using UnityEngine;
using System.Collections;
using System;

[System.Serializable]
public class CItem
{
	#region PUBLIC VARIABLES

	public enum EType
	{
		Junk,
		Weapon,
		Armor,
		Consumable,
		Quest
	}

	public int ID;
	public string Name;
	public string Description;
	public EType Type;
	public float Value;
	public Sprite Icon;

	public int StackSize;
	public int Amount;

	#endregion
	
	#region PROTECTED VARIABLES
	#endregion
	
	#region PRIVATE VARIABLES
	#endregion
	
	
	
	#region CONSTRUCTORS

	public CItem()
	{
		ID = -1;

		Name = "Unnamed";
		Description = "Undescribed";
		Type = EType.Junk;
		Value = 0.0f;
		Icon = null;

		StackSize = 0;
		Amount = 1;
	}

	public CItem(int _ID, string _Name, string _Description, EType _Type, float _Value, Sprite _Icon, int _StackSize = 0, int _Amount = 1)
	{
		ID = _ID;

		Name = _Name;
		Description = _Description;
		Type = _Type;
		Value = _Value;
		Icon = _Icon;

		StackSize = _StackSize;
		Amount = _Amount;
	}

	#endregion
	
	
	
	#region PUBLIC METHODS

	public CItem Copy()
	{
		return (CItem) this.MemberwiseClone();
	}

	#endregion
	
	
	
	#region PRIVATE METHODS
	#endregion
}

﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Basic class representing a killable creature.
/// </summary>
[SelectionBase]
public class CCreature : MonoBehaviour 
{
	#region PUBLIC VARIABLES

	public CFloatAndFade FadingText = null;
	public float MaximumHealth = 100.0f;

	#endregion
	
	#region PROTECTED VARIABLES

	protected Animator animator;

	#endregion
	
	#region PRIVATE VARIABLES
	#endregion

	#region PROPERTIES

	public float CurrentHealth
	{
		get;
		private set;
	}

	public float HealthPercentage
	{
		get { return CurrentHealth / MaximumHealth; }
	}

	#endregion
	
	
	
	#region MONOBEHAVIOUR

	protected virtual void Start()
	{
		animator = GetComponent<Animator>();
		CurrentHealth = MaximumHealth;
	}

	#endregion
	
	
	
	#region PUBLIC METHODS

	public bool AdjustHealth(float _Amount)
	{
		float before = CurrentHealth;

		// Adjust current health
		CurrentHealth = Mathf.Clamp(CurrentHealth + _Amount, 0.0f, MaximumHealth);

		if(animator != null)
			animator.Play("Hit");
		DisplayHealthChange(Mathf.Sign(_Amount), Mathf.Abs(CurrentHealth - before));

		// Check for death
		if(CurrentHealth <= 0.0f)
		{
			Die();
			return true;
		}

		Debug.Log(this.name + "::Creature: Adjusted health by " + _Amount + ", new health: " + CurrentHealth);

		return false;
	}

	#endregion



	#region PROTECTED METHODS

	protected virtual void Die()
	{
		Debug.Log("Creature " + this.name + " died.");
		Destroy(gameObject);
	}

	#endregion
	
	
	
	#region PRIVATE METHODS

	private void DisplayHealthChange(float _Sign, float _Amount)
	{
		if(FadingText == null)
			return;

		CFloatAndFade text = Instantiate(FadingText) as CFloatAndFade;
		text.transform.parent = GameObject.Find("Canvas").transform;

		if(_Sign >= 0.0f)
			text.Enable(transform, Vector3.up * 1.8f, _Amount.ToString(), Color.green);
		else
			text.Enable(transform, Vector3.up * 1.8f, _Amount.ToString(), Color.red);
	}

	#endregion
}

using UnityEngine;
using System.Collections;

/// <summary>
/// Class representing a combat-enabled character.
/// </summary>
public class CCharacter : CCreature 
{
	#region PUBLIC VARIABLES

	public CInventory Inventory = new CInventory();

	#endregion
	
	#region PROTECTED VARIABLES
	#endregion
	
	#region PRIVATE VARIABLES
	
	private bool isAttacking = false;
	private CWeapon weapon = null;

	#endregion

	#region PROPERTIES

	public bool IsAttacking
	{
		get { return isAttacking; }
		private set
		{
			isAttacking = value;

			if(animator != null)
				animator.SetBool("Attacking", isAttacking);
		}
	}

	#endregion
	
	
	
	#region MONOBEHAVIOUR

	protected override void Start ()
	{
		base.Start ();

		Inventory = new CInventory(25);

		weapon = GetComponentInChildren<CWeapon>();
		if(weapon != null)
			weapon.Initialize(this);
	}

	void Update()
	{
		if(CPlayerInput.This.GetButtonDown("Fire1"))
			Attack();

		if(CPlayerInput.This.GetButtonUp("Fire1"))
			AttackFinished();

		/*
		if(CPlayerInput.This.GetButtonDown("Fire2"))
			if(Inventory.AddItem(CItemFactory.GetRandom(CItem.EType.Junk)))
				Debug.Log("Item added.");
				*/

		if(CPlayerInput.This.GetButtonDown("Fire2"))
			Inventory.AddItem(CItemFactory.Get(0));

		if(CPlayerInput.This.GetButtonDown("Fire3"))
			Inventory.AddItem(CItemFactory.Get(1));
	}

	#endregion
	
	
	
	#region PUBLIC METHODS

	public bool CanAttack()
	{
		return (weapon != null && !isAttacking && animator.GetBool("Grounded") && !animator.GetCurrentAnimatorStateInfo(0).IsName(weapon.AnimationName));
	}

	public bool Attack()
	{
		if(CanAttack())
		{
			IsAttacking = true;
			CPlayerInput.This.LockAxes();

			if(animator != null)
				animator.Play(weapon.AnimationName);
			else
				Debug.LogError(this.name + "::CCharacter: Unable to attack without animator!");

			return true;
		}

		return false;
	}

	public void AttackFinished()
	{
		IsAttacking = false;
		CPlayerInput.This.UnlockAxes();
	}

	public void Fire()
	{
		if(weapon != null && !animator.IsInTransition(0))
			weapon.Fire();
	}

	public void FireFinished()
	{
		if(weapon != null)
			weapon.FireFinished();
	}

	#endregion
	
	
	
	#region PRIVATE METHODS
	#endregion
}

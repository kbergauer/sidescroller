﻿using UnityEngine;
using System.Collections;

public class CWeapon : MonoBehaviour 
{
	#region PUBLIC VARIABLES

	public float Damage = 1.0f;
	public string AnimationName = "";

	#endregion
	
	#region PROTECTED VARIABLES
	#endregion
	
	#region PRIVATE VARIABLES
	#endregion

	#region PROPERTIES

	public CCharacter Owner
	{
		get;
		private set;
	}

	#endregion
	
	
	
	#region MONOBEHAVIOUR
	#endregion
	
	
	
	#region PUBLIC METHODS

	public void Initialize(CCharacter _Owner)
	{
		Owner = _Owner;
	}

	public virtual void Fire()
	{
		Debug.Log(this.name + "::CWeapon - Fire");
	}
	
	public virtual void FireFinished()
	{
		Debug.Log(this.name + "::CWeapon - FireFinished");
	}

	#endregion
	
	
	
	#region PRIVATE METHODS
	#endregion
}

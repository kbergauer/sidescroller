﻿using UnityEngine;
using System.Collections;

public class CMeleeWeapon : CWeapon 
{
	#region PUBLIC VARIABLES
	#endregion
	
	#region PROTECTED VARIABLES
	#endregion
	
	#region PRIVATE VARIABLES
	#endregion
	
	
	
	#region MONOBEHAVIOUR

	void OnTriggerEnter2D(Collider2D _Collider)
	{
		Debug.Log(this.name + "::CMeleeWeapon - Trigger collision with " + _Collider.name);
		
		CCreature hit = _Collider.GetComponent<CCreature>();
		if(hit != null)
			hit.AdjustHealth(-Damage);
	}

	#endregion
	
	
	
	#region PUBLIC METHODS

	public override void Fire()
	{
		Debug.Log(this.name + "::CMeleeWeapon - Fire");

		collider2D.enabled = true;
	}
	
	public override void FireFinished()
	{
		Debug.Log(this.name + "::CMeleeWeapon - FireFinished");

		collider2D.enabled = false;
	}

	#endregion
	
	
	
	#region PRIVATE METHODS
	#endregion
}

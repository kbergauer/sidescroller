﻿using UnityEngine;
using System.Collections;

public class CMuzzleFlash : MonoBehaviour 
{
	#region PUBLIC VARIABLES

	public Sprite[] Sprites;

	#endregion
	
	#region PROTECTED VARIABLES
	#endregion
	
	#region PRIVATE VARIABLES

	private SpriteRenderer spriteRenderer;

	#endregion
	
	
	
	#region MONOBEHAVIOUR

	void Awake()
	{
		spriteRenderer = GetComponent<SpriteRenderer>();
	}

	void OnEnable()
	{
		spriteRenderer.sprite = Sprites[Random.Range(0, Sprites.Length)];
	}

	#endregion
	
	
	
	#region PUBLIC METHODS
	#endregion
	
	
	
	#region PRIVATE METHODS
	#endregion
}

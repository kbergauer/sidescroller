﻿using UnityEngine;
using System.Collections;

public class CRangedWeapon : CWeapon 
{
	#region PUBLIC VARIABLES

	public CProjectile Projectile;
	public float ProjectileSpeed = 10.0f;

	#endregion
	
	#region PROTECTED VARIABLES
	#endregion
	
	#region PRIVATE VARIABLES

	private Transform muzzle;

	#endregion
	
	
	
	#region MONOBEHAVIOUR

	void Start()
	{
		muzzle = transform.FindChild("Muzzle");
	}

	#endregion
	
	
	
	#region PUBLIC METHODS

	public override void Fire()
	{
		//Debug.Log(this.name + "::CRangedWeapon - Fire");

		muzzle.gameObject.SetActive(true);

		CProjectile projectile = Instantiate(Projectile, muzzle.position, muzzle.rotation) as CProjectile;
		projectile.transform.localScale = Owner.transform.localScale;
		projectile.Damage = Damage;
		projectile.Speed = ProjectileSpeed;
	}

	public override void FireFinished()
	{
		//Debug.Log(this.name + "::CRangedWeapon - FireFinished");

		muzzle.gameObject.SetActive(false);
	}

	#endregion
	
	
	
	#region PRIVATE METHODS
	#endregion
}

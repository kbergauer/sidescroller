﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Collider2D))]
public class CProjectile : MonoBehaviour 
{
	#region PUBLIC VARIABLES
	#endregion
	
	#region PROTECTED VARIABLES
	#endregion
	
	#region PRIVATE VARIABLES
	#endregion

	#region PROPERTIES

	public float Speed
	{
		get;
		set;
	}

	public float Damage
	{
		get;
		set;
	}

	public CCharacter Originator
	{
		get;
		set;
	}

	#endregion
	
	
	
	#region MONOBEHAVIOUR

	void Update()
	{
		transform.position += transform.right * transform.localScale.x * Speed * Time.deltaTime;
	}

	void OnCollisionEnter2D(Collision2D _Collision)
	{
		Debug.Log(this.name + "::CProjectile - Collision with " + _Collision.gameObject.name);
	}

	void OnTriggerEnter2D(Collider2D _Collider)
	{
		Debug.Log(this.name + "::CProjectile - Trigger collision with " + _Collider.name);

		CCreature hit = _Collider.GetComponent<CCreature>();
		if(hit != null)
			hit.AdjustHealth(-Damage);

		Destroy(gameObject);
	}

	#endregion
	
	
	
	#region PUBLIC METHODS
	#endregion
	
	
	
	#region PRIVATE METHODS
	#endregion
}

﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Class acting as a selection base for platforms.
/// </summary>
[SelectionBase]
public class CPlatform : MonoBehaviour 
{
	#region PUBLIC VARIABLES
	#endregion
	
	#region PROTECTED VARIABLES
	#endregion
	
	#region PRIVATE VARIABLES
	#endregion
	
	
	
	#region MONOBEHAVIOUR
	#endregion
	
	
	
	#region PUBLIC METHODS
	#endregion
	
	
	
	#region PRIVATE METHODS
	#endregion
}

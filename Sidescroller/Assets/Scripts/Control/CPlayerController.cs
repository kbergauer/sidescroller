﻿using UnityEngine;
using System.Collections;

[SelectionBase]
public class CPlayerController : MonoBehaviour
{
	// movement config
	public float gravity = -25f;
	public float runSpeed = 8f;
	public float groundDamping = 20f; // how fast do we change direction? higher means faster
	public float inAirDamping = 5f;
	public float jumpHeight = 3f;
	
	private float normalizedHorizontalSpeed = 0.0f;

	private CPlayerInput _input;
	private CCharacterController2D _controller;
	private Animator _animator;
	private RaycastHit2D _lastControllerColliderHit;
	private Vector3 _velocity;

	private int currentFloorLayer;


	void Awake()
	{
		_input = GetComponent<CPlayerInput>();
		_controller = GetComponent<CCharacterController2D>();
		_animator = GetComponent<Animator>();

		// listen to some events for illustration purposes
		_controller.onControllerCollidedEvent += onControllerCollider;
		//_controller.onTriggerEnterEvent += onTriggerEnterEvent;
		//_controller.onTriggerExitEvent += onTriggerExitEvent;
	}


	#region Event Listeners

	void onControllerCollider( RaycastHit2D hit )
	{
		// bail out on plain old ground hits cause they arent very interesting
		if( hit.normal.y > 0.95f && hit.normal.y < 1.05f )
		{
			currentFloorLayer = hit.collider.gameObject.layer;
			return;
		}

		// logs any collider hits if uncommented. it gets noisy so it is commented out for the demo
		//Debug.Log( "flags: " + _controller.collisionState + ", hit.normal: " + hit.normal );
	}

	/*
	void onTriggerEnterEvent( Collider2D col )
	{
		//Debug.Log( "onTriggerEnterEvent: " + col.gameObject.name );
	}


	void onTriggerExitEvent( Collider2D col )
	{
		//Debug.Log( "onTriggerExitEvent: " + col.gameObject.name );
	}
	*/

	#endregion


	// the Update loop contains a very simple example of moving the character around and controlling the animation
	void Update()
	{
		// grab our current _velocity to use as a base for all calculations
		_velocity = _controller.velocity;

		if( _controller.isGrounded )
		{
			_animator.SetBool("Grounded", true);
			_velocity.y = 0;
		}
		else
			_animator.SetBool("Grounded", false);

		// get axis input
		normalizedHorizontalSpeed = _input.GetAxis("Horizontal");
		_animator.SetFloat("hSpeed", Mathf.Abs(normalizedHorizontalSpeed));

		// flip character in movement direction
		if(transform.localScale.x < 0.0f && normalizedHorizontalSpeed > 0.0f)
			transform.localScale = new Vector3( -transform.localScale.x, transform.localScale.y, transform.localScale.z );
		else if(transform.localScale.x > 0.0f && normalizedHorizontalSpeed < 0.0f)
			transform.localScale = new Vector3( -transform.localScale.x, transform.localScale.y, transform.localScale.z );

		/* old movement
		if( Input.GetKey( KeyCode.RightArrow ) )
		{
			normalizedHorizontalSpeed = 1;
			if( transform.localScale.x < 0f )
				transform.localScale = new Vector3( -transform.localScale.x, transform.localScale.y, transform.localScale.z );

			//if( _controller.isGrounded )
				//_animator.Play( Animator.StringToHash( "Run" ) );
		}
		else if( Input.GetKey( KeyCode.LeftArrow ) )
		{
			normalizedHorizontalSpeed = -1;
			if( transform.localScale.x > 0f )
				transform.localScale = new Vector3( -transform.localScale.x, transform.localScale.y, transform.localScale.z );

			//if( _controller.isGrounded )
				//_animator.Play( Animator.StringToHash( "Run" ) );
		}
		else
		{
			normalizedHorizontalSpeed = 0;

			//if( _controller.isGrounded )
				//_animator.Play( Animator.StringToHash( "Idle" ) );
		}
		*/

		// we can only jump whilst grounded
		if( _controller.isGrounded && _input.GetButtonDown("Jump") )
		{
			// drop through one way platforms
			if( _controller.isGrounded && currentFloorLayer == LayerMask.NameToLayer("GroundOneWay") && _input.GetAxis("Vertical") < 0.0f)
			{
				//_animator.Play( Animator.StringToHash( "Jump" ) );
				Vector3 position = transform.position;
				position.y -= 0.1f;
				transform.position = position;
			}
			// otherwise jump
			else
			{
				_velocity.y = Mathf.Sqrt( 2f * jumpHeight * -gravity );
				//_animator.Play( Animator.StringToHash( "Jump" ) );
			}
		}

		// apply horizontal speed smoothing it
		var smoothedMovementFactor = _controller.isGrounded ? groundDamping : inAirDamping; // how fast do we change direction?
		_velocity.x = Mathf.Lerp( _velocity.x, normalizedHorizontalSpeed * runSpeed, Time.deltaTime * smoothedMovementFactor );

		// apply gravity before moving
		_velocity.y += gravity * Time.deltaTime;

		_controller.move( _velocity * Time.deltaTime );

		_animator.SetFloat("vSpeed", _controller.velocity.y);
	}

}

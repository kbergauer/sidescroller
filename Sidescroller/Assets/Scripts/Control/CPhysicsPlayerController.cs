﻿using UnityEngine;
using System.Collections;

public class CPhysicsPlayerController : MonoBehaviour 
{
	#region PUBLIC VARIABLES

	public float MaximumSpeed = 10.0f;
	public float JumpForce = 700.0f;
	public Transform GroundCheck;
	public LayerMask GroundLayer;

	#endregion
	
	#region PROTECTED VARIABLES
	#endregion
	
	#region PRIVATE VARIABLES

	private Animator animator = null;
	private bool facingRight = true;
	private bool grounded = false;
	private float groundRadius = 0.2f;

	#endregion
	
	
	
	#region MONOBEHAVIOUR

	void Start()
	{
		animator = GetComponent<Animator>();
	}

	void FixedUpdate()
	{
		grounded = Physics2D.OverlapCircle(GroundCheck.position, groundRadius, GroundLayer);
		animator.SetBool("Grounded", grounded);

		float move = Input.GetAxis("Horizontal");

		animator.SetFloat("hSpeed", Mathf.Abs(move));

		rigidbody2D.velocity = new Vector2(move * MaximumSpeed, rigidbody2D.velocity.y);

		if (move > 0 && !facingRight)
			Flip();
		else if (move < 0 && facingRight)
			Flip();
	}

	void Update()
	{
		if (grounded && Input.GetButtonDown("Jump"))
		{
			animator.SetBool("Grounded", false);
			rigidbody2D.AddForce(new Vector2(0.0f, JumpForce));
		}
	}

	#endregion
	
	
	
	#region PUBLIC METHODS
	#endregion
	
	
	
	#region PRIVATE METHODS

	private void Flip()
	{
		facingRight = !facingRight;
		Vector3 scale = transform.localScale;
		scale.x *= -1.0f;
		transform.localScale = scale;
	}

	#endregion
}

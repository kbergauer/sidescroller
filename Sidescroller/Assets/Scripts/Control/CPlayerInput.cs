﻿using UnityEngine;
using System.Collections;

public class CPlayerInput : MonoBehaviour 
{
	#region PUBLIC VARIABLES

	public enum EDevice
	{
		Any,
		Keyboard,
		Controller1,
		Controller2,
		Controller3,
		Controller4
	};

	public EDevice Device = EDevice.Keyboard;

	#endregion
	
	#region PROTECTED VARIABLES
	#endregion
	
	#region PRIVATE VARIABLES

	private static CPlayerInput instance;

	private string[] devicePrefix = new string[] { "", "KB_", "C1_", "C2_", "C3_", "C4_" };
	private bool ignoreAxes = false;
	private bool ignoreButtons = false;

	#endregion

	#region PROPERTIES

	public static CPlayerInput This
	{
		get 
		{ 
			if(instance == null) 
				instance = FindObjectOfType(typeof(CPlayerInput)) as CPlayerInput;

			return instance; 
		}
	}

	#endregion
	
	
	
	#region MONOBEHAVIOUR
	#endregion
	
	
	
	#region PUBLIC METHODS

	public void Lock()
	{
		LockAxes();
		LockButtons();

		//Debug.Log(this.name + "::CPlayerInput - Locked.");
	}

	public void Unlock()
	{
		UnlockAxes();
		UnlockButtons();

		//Debug.Log(this.name + "::CPlayerInput - Unlocked.");
	}

	public void LockAxes()
	{
		ignoreAxes = true;
	}

	public void UnlockAxes()
	{
		ignoreAxes = false;
	}

	public void LockButtons()
	{
		ignoreButtons = true;
	}

	public void UnlockButtons()
	{
		ignoreButtons = false;
	}

	public float GetAxis(string _Axis)
	{
		float axis = 0.0f;

		if(!ignoreAxes)
			axis = Input.GetAxis(devicePrefix[(int)Device] + _Axis);

		return axis;
	}

	public float GetAxisRaw(string _Axis)
	{
		float axis = 0.0f;

		if(!ignoreAxes)
			axis = Input.GetAxisRaw(devicePrefix[(int)Device] + _Axis);

		return axis;
	}

	public bool GetButton(string _Button)
	{
		return (ignoreButtons ? false : Input.GetButton(devicePrefix[(int)Device] + _Button));
	}

	public bool GetButtonDown(string _Button)
	{
		return (ignoreButtons ? false : Input.GetButtonDown(devicePrefix[(int)Device] + _Button));
	}

	public bool GetButtonUp(string _Button)
	{
		return (ignoreButtons ? false : Input.GetButtonUp(devicePrefix[(int)Device] + _Button));
	}

	#endregion
	
	
	
	#region PRIVATE METHODS
	#endregion
}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CTooltip : MonoBehaviour 
{
	#region PUBLIC VARIABLES

	public Text Name;
	public Text Description;

	#endregion
	
	#region PROTECTED VARIABLES
	#endregion
	
	#region PRIVATE VARIABLES
	#endregion
	
	
	
	#region MONOBEHAVIOUR
	#endregion
	
	
	
	#region PUBLIC METHODS

	public void SetContent(string _Name, string _Description)
	{
		Name.text = _Name;
		Description.text = _Description;
	}

	#endregion
	
	
	
	#region PRIVATE METHODS
	#endregion
}

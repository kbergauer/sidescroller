﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CWindow : MonoBehaviour 
{
	#region PUBLIC VARIABLES

	public bool Dragable = true;
	public bool Foldable = true;

	public GameObject ContentPanel = null;
	public GameObject TitleBar = null;

	#endregion
	
	#region PROTECTED VARIABLES
	#endregion
	
	#region PRIVATE VARIABLES

	private Animator animator;
	private RectTransform rectTransform;
	private Button fold;
	private Button unfold;

	private Vector3 position;
	private Vector3 offset;
	private bool isFolded = false;

	#endregion

	#region PROPERTIES

	public bool IsFolded
	{
		get { return isFolded; }
		private set
		{
			isFolded = value;

			if(animator != null)
				animator.SetBool("IsFolded", isFolded);
			else
				ContentPanel.SetActive(!isFolded);
		}
	}

	#endregion

	
	
	#region MONOBEHAVIOUR

	void Awake()
	{
		animator = GetComponent<Animator>();
		rectTransform = GetComponent<RectTransform>();

		if(TitleBar != null)
		{
			Transform t = null;

			t = TitleBar.transform.FindChild("Fold");
			if(t != null)
				fold = t.GetComponent<Button>();

			t = TitleBar.transform.FindChild("Unfold");
			if(t != null)
			{
				unfold = t.GetComponent<Button>();
				unfold.gameObject.SetActive(false);
			}

			if(!Foldable)
			{
				if(fold != null)
					fold.interactable = false;
				if(unfold != null)
					unfold.interactable = false;
			}
		}

		IsFolded = false;
	}

	#endregion
	
	
	
	#region PUBLIC METHODS

	public void Grab()
	{
		offset = transform.position - Input.mousePosition;
		position = transform.position;
	}

	public void Drop()
	{
		offset = Vector3.zero;
	}

	public void Drag()
	{
		if(!Dragable)
			return;

		position = Input.mousePosition + offset;
		position.x = Mathf.Clamp(position.x, rectTransform.rect.width / 2.0f, Screen.width - rectTransform.rect.width / 2.0f);

		if(!IsFolded)
			position.y = Mathf.Clamp(position.y, rectTransform.rect.height, Screen.height);
		else
			position.y = Mathf.Clamp(position.y, ((RectTransform)TitleBar.transform).rect.height, Screen.height);

		transform.position = position;
	}

	public void Fold()
	{
		// Switch buttons
		fold.gameObject.SetActive(false);
		unfold.gameObject.SetActive(true);

		// Hide content
		//ContentPanel.SetActive(false);

		IsFolded = true;
	}

	public void Unfold()
	{
		// Switch buttons
		unfold.gameObject.SetActive(false);
		fold.gameObject.SetActive(true);

		// Correct position
		position = transform.position;
		position.y = Mathf.Clamp(position.y, rectTransform.rect.height, Screen.height);
		transform.position = position;

		// Show content
		//ContentPanel.SetActive(true);

		IsFolded = false;
	}

	public void Close()
	{
		gameObject.SetActive(false);
	}

	#endregion
	
	
	
	#region PRIVATE METHODS
	#endregion
}

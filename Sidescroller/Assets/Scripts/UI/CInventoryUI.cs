﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

public class CInventoryUI : MonoBehaviour 
{
	#region PUBLIC VARIABLES

	public CCharacter Target;

	public Image DraggedItemImage;
	public CTooltip Tooltip;

	#endregion
	
	#region PROTECTED VARIABLES
	#endregion
	
	#region PRIVATE VARIABLES

	private CInventorySlot[] slots;
	
	private CItem draggedItem = null;
	private int sourceSlot = -1;
	private int destinationSlot = -1;

	#endregion

	#region PROPERTIES

	public bool IsDragging
	{
		get;
		private set;
	}

	#endregion

	
	
	#region MONOBEHAVIOUR

	void Start()
	{
		if(Target == null)
		{
			enabled = false;
			return;
		}

		Target.Inventory.OnAddItem += AddItem;
		Target.Inventory.OnUpdateItemStack += UpdateItemStack;

		slots = GetComponentsInChildren<CInventorySlot>();
		for(int i = 0; i < slots.Length; i++)
			slots[i].SetUI(this);

		IsDragging = false;
	}

	void Update()
	{
		if(IsDragging)
			DraggedItemImage.rectTransform.position = Input.mousePosition;
	}

	#endregion
	
	
	
	#region PUBLIC METHODS

	public void AddItem(CItem _Item)
	{
		// Get empty slot
		for(int i = 0; i < slots.Length; i++)
		{
			if(slots[i].Item == null)
			{
				// Set item
				slots[i].SetItem(_Item);
				break;
			}
		}
	}

	public void UpdateItemStack(CItem _Item)
	{
		for(int i = 0; i < slots.Length; i++)
		{
			if(slots[i].Item == _Item && slots[i].Item.Amount > 1)
			{
				Text size = slots[i].transform.GetChild(1).GetComponent<Text>();
				size.text = slots[i].Item.Amount.ToString();
				size.gameObject.SetActive(true);
			}
		}
	}

	public void StartDrag(CInventorySlot _Slot)
	{
		//Debug.Log(this.name + ": Start drag.");

		draggedItem = _Slot.Item;
		sourceSlot = Array.IndexOf(slots, _Slot);
		slots[sourceSlot].ClearItem();

		DraggedItemImage.sprite = draggedItem.Icon;
		if(draggedItem.StackSize > 0 && draggedItem.Amount > 0)
		{
			Text size = DraggedItemImage.transform.GetChild(0).GetComponent<Text>();
			size.text = draggedItem.Amount.ToString();
			size.gameObject.SetActive(true);
		}
		DraggedItemImage.gameObject.SetActive(true);

		IsDragging = true;
	}

	public void StopDrag(CInventorySlot _Slot)
	{
		//Debug.Log(this.name + ": Stop drag.");

		destinationSlot = Array.IndexOf(slots, _Slot);

		if(slots[destinationSlot].Item != null)
			slots[sourceSlot].SetItem(slots[destinationSlot].Item);
		slots[destinationSlot].SetItem(draggedItem);

		if(slots[sourceSlot].Item == null || sourceSlot == destinationSlot)
		{
			draggedItem = null;
			DraggedItemImage.gameObject.SetActive(false);
			Transform size = DraggedItemImage.transform.GetChild(0);
			if(size != null && size.gameObject.activeInHierarchy)
				size.gameObject.SetActive(false);

			IsDragging = false;
		}
		else
			StartDrag(slots[sourceSlot]);
	}

	public void ShowTooltip(CInventorySlot _Slot)
	{
		Tooltip.SetContent(_Slot.Item.Name + " (" + _Slot.Item.ID + ")", _Slot.Item.Description);
	
		Tooltip.transform.position = _Slot.transform.position - new Vector3(-_Slot.GetComponent<RectTransform>().rect.width / 2.0f, -_Slot.GetComponent<RectTransform>().rect.height / 2.0f, 0.0f);
		Tooltip.gameObject.SetActive(true);
	}

	public void HideTooltip()
	{
		Tooltip.gameObject.SetActive(false);
	}

	#endregion
	
	
	
	#region PRIVATE METHODS
	#endregion
}

﻿using UnityEngine;
using System.Collections;

public class CPlayerHUD : MonoBehaviour 
{
	#region PUBLIC VARIABLES

	public CCreature Player = null;
	
	public RectTransform HealthbarContent = null;

	#endregion
	
	#region PROTECTED VARIABLES
	#endregion
	
	#region PRIVATE VARIABLES

	private float healthbarWidth = 0.0f;

	#endregion
	
	
	
	#region MONOBEHAVIOUR

	void Awake()
	{
		if(HealthbarContent != null)
			healthbarWidth = HealthbarContent.rect.width;
	}

	void Update()
	{
		if(Player == null)
		{
			Debug.LogWarning(this.name + "::PlayerHUD: Missing player to track, disabling.");
			this.enabled = false;
			return;
		}

		if(HealthbarContent != null)
			HealthbarContent.sizeDelta = new Vector2(healthbarWidth * Player.HealthPercentage, HealthbarContent.sizeDelta.y);
	}

	#endregion

	
	
	#region PUBLIC METHODS
	#endregion
	
	
	
	#region PRIVATE METHODS
	#endregion
}

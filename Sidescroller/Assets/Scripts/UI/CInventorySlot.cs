﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class CInventorySlot : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler
{
	#region PUBLIC VARIABLES

	public CItem Item;
	public Image Icon;
	public Text Size;

	#endregion
	
	#region PROTECTED VARIABLES
	#endregion
	
	#region PRIVATE VARIABLES

	private CInventoryUI ui;

	#endregion
	
	
	
	#region MONOBEHAVIOUR

	void Start()
	{
		Item = null;
		Icon = transform.GetChild(0).GetComponent<Image>();
		Size = transform.GetChild(1).GetComponent<Text>();
	}

	public void OnPointerEnter(PointerEventData _Data)
	{
		if(ui == null)
			return;

		if(!ui.IsDragging && Item != null)
			ui.ShowTooltip(this);
	}

	public void OnPointerExit(PointerEventData _Data)
	{
		if(ui == null)
			return;
		
		ui.HideTooltip();
	}
	
	public void OnPointerDown(PointerEventData _Data)
	{
		if(ui == null)
			return;

		// Pickup
		if(!ui.IsDragging)
		{
			if(Item != null)
				ui.StartDrag(this);
		}
		// Drop
		else
		{
			ui.StopDrag(this);
		}
	}

	#endregion
	
	
	
	#region PUBLIC METHODS

	public void SetUI(CInventoryUI _UI)
	{
		ui = _UI;
	}

	public CItem SetItem(CItem _Item)
	{
		CItem old = Item;

		Item = _Item;
		Icon.sprite = Item.Icon;
		Icon.gameObject.SetActive(true);

		if(Item.StackSize > 0)
		{
			Size.text = Item.Amount.ToString();
			Size.gameObject.SetActive(true);
		}

		return old;
	}

	public void ClearItem()
	{
		Item = null;
		Icon.gameObject.SetActive(false);
		Size.gameObject.SetActive(false);
	}

	#endregion
	
	
	
	#region PRIVATE METHODS
	#endregion
}

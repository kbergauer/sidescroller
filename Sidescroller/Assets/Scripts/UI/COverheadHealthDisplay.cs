﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class COverheadHealthDisplay : MonoBehaviour 
{
	#region PUBLIC VARIABLES
	
	public CCreature Target = null;

	public RectTransform Display = null;
	public Vector3 DisplayOffset = Vector3.up * 1.8f;
	public bool HideHealthIfFull = true;
	public bool HideName = false;
	
	#endregion
	
	#region PROTECTED VARIABLES
	#endregion
	
	#region PRIVATE VARIABLES

	private RectTransform display = null;
	private RectTransform healthbar = null;
	private RectTransform healthbarContent = null;
	private Text nameDisplay = null;
	private float healthbarWidth = 0.0f;
	
	#endregion
	
	
	
	#region MONOBEHAVIOUR
	
	void Start()
	{
		if(Target == null)
			Target = GetComponent<CCreature>();

		GameObject canvas = GameObject.Find("Canvas");

		display = Instantiate(Display) as RectTransform;
		display.name = Target.name + "Display";
		display.transform.parent = canvas.transform;

		healthbar = display.transform.GetChild(0) as RectTransform;
		healthbarContent = healthbar.transform.GetChild(1) as RectTransform;

		nameDisplay = display.GetComponentInChildren<Text>();
		nameDisplay.text = Target.name;

		if(HideName)
			nameDisplay.enabled = false;

		if(healthbarContent != null)
			healthbarWidth = healthbarContent.rect.width;
	}
	
	void Update()
	{
		if(Target == null)
		{
			Debug.LogWarning(this.name + "::PlayerHUD: Missing player to track, disabling.");

			Destroy(display);

			this.enabled = false;
			return;
		}

		if(display != null)
		{
			// Set display position
			display.position = Camera.main.WorldToScreenPoint(Target.transform.position + DisplayOffset);
		}
		
		if(healthbar != null && healthbarContent != null)
		{
			if(HideHealthIfFull)
			{
				if(Target.HealthPercentage == 1.0f)
				{
					if(healthbar.gameObject.activeInHierarchy)
						healthbar.gameObject.SetActive(false);
				}
				else if(!healthbar.gameObject.activeInHierarchy)
					healthbar.gameObject.SetActive(true);
			}

			// Set health display
			healthbarContent.sizeDelta = new Vector2(healthbarWidth * Target.HealthPercentage, healthbarContent.sizeDelta.y);
		}
	}

	void OnDestroy()
	{
		if(display != null && display.gameObject != null)
			Destroy(display.gameObject);
	}
	
	#endregion
	
	
	
	#region PUBLIC METHODS
	#endregion
	
	
	
	#region PRIVATE METHODS
	#endregion
}

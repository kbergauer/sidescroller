﻿using UnityEngine;
using System.Collections;

public class CDisplayControl : MonoBehaviour 
{
	#region PUBLIC VARIABLES

	[System.Serializable]
	public struct SPair
	{
		public GameObject Display;
		public string Button;
	};

	public SPair[] Pairs;

	#endregion
	
	#region PROTECTED VARIABLES
	#endregion
	
	#region PRIVATE VARIABLES
	#endregion
	
	
	
	#region MONOBEHAVIOUR

	void Update()
	{
		for(int i = 0; i < Pairs.Length; i++)
		{
			if(Pairs[i].Button.Length > 0 && Pairs[i].Display != null)
			{
				if(CPlayerInput.This.GetButtonDown(Pairs[i].Button))
					Pairs[i].Display.SetActive(!Pairs[i].Display.activeInHierarchy);
			}
		}
	}

	#endregion
	
	
	
	#region PUBLIC METHODS
	#endregion
	
	
	
	#region PRIVATE METHODS
	#endregion
}

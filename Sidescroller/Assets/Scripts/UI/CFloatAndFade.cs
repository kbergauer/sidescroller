﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CFloatAndFade : MonoBehaviour 
{
	#region PUBLIC VARIABLES

	public Vector3 Direction = Vector3.up;
	public float FloatSpeed = 1.0f;
	public float FadeSpeed = 1.0f;

	#endregion
	
	#region PROTECTED VARIABLES
	#endregion
	
	#region PRIVATE VARIABLES

	private bool isSet = false;
	private Transform parent = null;
	private Text text;
	private Color color;
	private Vector3 offset;

	#endregion
	
	
	
	#region MONOBEHAVIOUR

	void Awake()
	{
		text = GetComponent<Text>();
		if(text == null)
			Destroy(gameObject);

		color = text.color;
	}

	void Update()
	{
		if(!isSet)
			return;

		offset += Direction * FloatSpeed * Time.deltaTime;
		//Debug.Log("Offset: " + offset);
		if(parent != null)
			transform.position = Camera.main.WorldToScreenPoint(parent.position + offset);
		else
			transform.position = transform.position + offset;

		color.a -= FadeSpeed * Time.deltaTime;
		text.color = color;

		if(text.color.a <= 0.0f)
			Destroy(gameObject);
	}

	#endregion
	
	
	
	#region PUBLIC METHODS

	public void Enable(Transform _Parent, string _Text)
	{
		text.text = _Text;
		parent = _Parent;

		transform.position = Camera.main.WorldToScreenPoint(parent.position + offset);

		text.enabled = true;
		isSet = true;
	}

	public void Enable(Transform _Parent, Vector3 _InitialOffset, string _Text)
	{
		offset = _InitialOffset;
		Enable(_Parent, _Text);
	}

	public void Enable(Transform _Parent, string _Text, Color _Color)
	{
		color = _Color;
		Enable(_Parent, _Text);
	}

	public void Enable(Transform _Parent, Vector3 _InitialOffset, string _Text, Color _Color)
	{
		offset = _InitialOffset;
		Enable(_Parent, _Text, _Color);
	}

	#endregion
	
	
	
	#region PRIVATE METHODS
	#endregion
}

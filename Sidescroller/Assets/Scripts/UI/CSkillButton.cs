﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CSkillButton : MonoBehaviour 
{
	#region PUBLIC VARIABLES

	[System.Serializable]
	public struct SConnection
	{
		public CSkillButton Skill;
		public Image[] Lines;
	}

	public CSkillButton LastSkill;

	public SConnection[] NextSkills;

	public Color EnabledConnection = Color.white;
	public Color DisabledConnection = Color.gray;
	public Color ActiveConnection = Color.green;

	#endregion
	
	#region PROTECTED VARIABLES
	#endregion
	
	#region PRIVATE VARIABLES
	#endregion
	
	
	
	#region MONOBEHAVIOUR
	#endregion
	
	
	
	#region PUBLIC METHODS

	public void Skill(bool _Enable)
	{
		if(_Enable)
		{
			if(LastSkill != null)
				LastSkill.ActivateConnection(this);

			EnableConnections();
		}
		else
		{
			if(LastSkill != null)
			{
				//Debug.Log(this.name + ": Last skill not null: " + LastSkill.name);
				LastSkill.DeactivateConnection(this);
			}

			DisableConnections();
		}
	}

	public void EnableConnections()
	{
		for(int i = 0; i < NextSkills.Length; i++)
		{
			for(int j = 0; j < NextSkills[i].Lines.Length; j++)
				NextSkills[i].Lines[j].color = EnabledConnection;

			NextSkills[i].Skill.GetComponent<Toggle>().interactable = true;
		}
	}

	public void DisableConnections()
	{
		//Debug.Log(this.name + ": Disabling connections.");

		for(int i = 0; i < NextSkills.Length; i++)
		{
			for(int j = 0; j < NextSkills[i].Skill.NextSkills.Length; j++)
				NextSkills[i].Skill.NextSkills[j].Skill.DisableConnections();

			NextSkills[i].Skill.GetComponent<Toggle>().isOn = false;
			NextSkills[i].Skill.GetComponent<Toggle>().interactable = false;

			for(int j = 0; j < NextSkills[i].Lines.Length; j++)
			{
				NextSkills[i].Lines[j].color = DisabledConnection;
				//Debug.Log("Disabling line " + NextSkills[i].Lines[j].name);
			}
		}
	}

	public void ActivateConnection(CSkillButton _Skill)
	{
		for(int i = 0; i < NextSkills.Length; i++)
		{
			if(NextSkills[i].Skill == _Skill)
			{
				for(int j = 0; j < NextSkills[i].Lines.Length; j++)
					NextSkills[i].Lines[j].color = ActiveConnection;
			}
		}
	}

	public void DeactivateConnection(CSkillButton _Skill)
	{
		//Debug.Log(this.name + ": Deactivate connection to " + _Skill.name);

		for(int i = 0; i < NextSkills.Length; i++)
		{
			if(NextSkills[i].Skill == _Skill)
			{
				for(int j = 0; j < NextSkills[i].Lines.Length; j++)
				{
					NextSkills[i].Lines[j].color = EnabledConnection;
					//Debug.Log("Enabling line " + NextSkills[i].Lines[j].name);
				}
			}
		}
	}

	#endregion
	
	
	
	#region PRIVATE METHODS
	#endregion
}
